import 'package:consumer_test_s/ui/home/view_model_home.dart';
import 'package:consumer_test_s/ui/home/widget/widget_text_length.dart';
import 'package:consumer_test_s/ui/home/widget/wiget_list_name.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

// ignore: must_be_immutable
class ViewHome extends StatelessWidget {
  ViewHome({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _viewModelHome = Provider.of<ViewModelHome>(context);
    print('only run once');

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
            "value = ${_viewModelHome.getListName()[_viewModelHome.getListName().length - 1]} ...... index = ${_viewModelHome.getListName().length - 1}"),
      ),
      body: ListName(),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          _viewModelHome.incrementCounter();
        },
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ),
    );
  }
}
