import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:consumer_test_s/ui/home/view_model_home.dart';

class TextLength extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Consumer<ViewModelHome>(
      builder: (context, names, child) {
        return Text("${names.getListName().length}");
      },
    );
  }
}
