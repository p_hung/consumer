import 'package:consumer_test_s/ui/home/view_model_home.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ListName extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer<ViewModelHome>(
      builder: (context, names, state) {
        return ListView.builder(
          itemCount: names.getListName().length,
          itemBuilder: (BuildContext context, int index) {
            return Center(
              child: Text(
                "${names.getListName()[index]}",
                style: Theme.of(context).textTheme.display1,
              ),
            );
          },
        );
      },
    );
  }
}
