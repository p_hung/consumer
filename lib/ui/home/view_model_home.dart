import 'dart:math';

import 'package:flutter/material.dart';

class ViewModelHome extends ChangeNotifier {
  Random rd = Random();
  List<String> _names = ['dog', 'cat'];

  List<String> getListName() {
    return _names;
  }

  void addName() {
    _names.add("index ${rd.nextInt(20)}");
  }

  incrementCounter() {
    addName();
    notifyListeners();
  }
}
