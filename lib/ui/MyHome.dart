import 'package:consumer_test_s/ui/home/view_home.dart';
import 'package:consumer_test_s/ui/home/view_model_home.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class MyHome extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: ChangeNotifierProvider<ViewModelHome>(
          builder: (_) => ViewModelHome(),
          child: ViewHome(),
        ));
  }
}
